import userModel from '../api/users/userModel';
import users from '../initialise-dev/users';
import dotenv from 'dotenv';
// import genreModel from '../api/genres/genreModel'
import movieModel from '../api/movies/movieModel';
import movies from '../initialise-dev/movies';
import people from './people';
import peopleModel from '../api/people/peopleModel';
import TV from './TV';
import TVModel from '../api/TV/TVModel';


dotenv.config();

// deletes all movies documents in collection and inserts test data

if (process.env.NODE_ENV === 'development') {
  loadUsers();
  loadMovies();
  loadPeople();
  loadTV();
}