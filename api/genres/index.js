import express from 'express';
import Genre from './genresModel'
import asyncHandler from 'express-async-handler';
import { getGenres } from '../tmdb-api';
const router = express.Router(); // eslint-disable-line

// Get all users
router.get('/', async (req, res) => {
    const genres = await Genre.find();
    res.status(200).json(genres);
});

router.get('/api/genres/tmdb/movies', asyncHandler(async (req, res) => {
  const genres = await getGenres();
  res.status(200).json(genres);
}));

export default router;