import mongoose from 'mongoose';
//import bcrypt from 'bcrypt';
const Schema = mongoose.Schema;


  
  const UserSchema = new Schema({
    username: { type: String, unique: true, required: true},
    password: {type: String, required: true },
    favorites: [{type: Number}]
  });
  UserSchema.statics.findByUserName = function (username) {
    return this.findOne({ username: username });
  };
  

  UserSchema.pre('save', function(next) {
    const user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, (err, salt)=> {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, (err, hash)=> {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});
export default mongoose.model('User', UserSchema);