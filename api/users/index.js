import express from 'express';
import User from './userModel';
import asyncHandler from 'express-async-handler';
import jwt from 'jsonwebtoken';
//import movieModel from '../movies/movieModel';
const router = express.Router(); // eslint-disable-line

// Get all users
router.get('/', async (req, res) => {
    const users = await User.find();
    res.status(200).json(users);
});

// register
router.post('/',asyncHandler( async (req, res, next) => {
    if (!req.body.username || !req.body.password) {
      res.status(401).json({success: false, msg: 'Please pass username and password.'});
      return next();
    }
    if (req.query.action === 'register') {
      var username = req.body.username;
      var password = req.body.password;
      if (username.match( /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/)||password.match(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)){
        res.status(201).json({code: 201, msg: 'Successful created new user.'});
      
      }
      else{
        res.status(401).json({code: 401,msg: 'Authentication failed. Wrong password.'});
      }
      await User.create(req.body);
      res.status(201).json({code: 201, msg: 'Successful created new user.'});
    } else {
      const user = await User.findByUserName(req.body.username);
        if (!user) return res.status(401).json({ code: 401, msg: 'Authentication failed. User not found.' });
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (isMatch && !err) {
            // if user is found and password matches, create a token
            const token = jwt.sign(user.username, process.env.SECRET);
            // return the information including token as JSON
            res.status(200).json({success: true, token: 'BEARER ' + token});
          } else {
            res.status(401).json({code: 401,msg: 'Authentication failed. Wrong password.'});
          }
        });
      }
  }));

router.post('/:userName/favorites', asyncHandler(async (req, res) => {
  
  const newFavorite = req.body.newFavorite;
  const userName = req.params.userName;

  const user = await User.findByUserName(userName);

  if (user.favorites.includes(newFavorite)) {
  

      res.status(201).json({code: 201, msg: 'Already exists in favorites.'})
  } else {
      await user.favorites.push(newFavorite);
      await user.save();
      console.log(user) 
      res.status(201).json(user); 
  }
}));


export default router;