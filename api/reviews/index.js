import express from 'express';
import Review from './reviewModel'
const router = express.Router(); 
// Get movie reviews
router.get('/', async (req, res) => {
    const review = await Review.find();
    res.status(200).json(review);
});

export default router;