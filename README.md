# Assignment 2 - Agile Software Practice.

Name: Sirui Yao

## API endpoints.

GET /api/movies/tmdb/movies/:id/reviews -get the review of some movies

POST /api/movies/tmdb/movies/:id/reviews -add the movie into movie list

GET /api/movies/:id/ -get the details of some movies

GET /api/movies/tmdb/movies/:id/images -get movies images of the movies

GET /api/movies/tmdb/upcoming -get twenty upcoming movies to list in the upcoming page

GET /api/movies/tmdb/toprated -get some toprated movies to list in the toprated page

GET /api/people//tmdb/popular -get popular people to the peoplepage

GET /api/people/tmdb/people/:id/ -get people's details to the peopledetail page

GET /api/people/tmdb/people/:id/images -get people's images to the peopledetail page

GET /api/TV/tmdb/popular -get twenty TVs to the TV page

GET /api/TV/tmdb/TV/:id/ -get TV's detail to the TVdetail page

GET /api/TV/tmdb/TV/:id/similar -get TV's similar images to the TVdetail page

GET /api/TV/tmdb/TV/:id/reviews -get TV's reviews to the TV in the app

GET /api/TV/tmdb/TV/:id/genres -get TV's genres to the TV in the app

GET /api/genres -get all of the genres about the movies

PUT /api/genres/:id/ -update the movie genres

GET /api/users -get the user information for the app

POST /api/users -add the new user to the app

PUT /api/users/:id/ -update the users information

## Automated Testing.

 Users endpoint

    POST /api/users

      For a register action

        when the payload is correct

database connected to movies on ac-udxuru6-shard-00-00.o9kvy8e.mongodb.net

          √ should return a 201 status and the confirmation message

        when the payload is wrong

          √ should return a 401 status and the remind message

      when the password is incorrect

        √ should return a 401 status and an error message

      when the username and passname is no pass

        √ should return a 401 status and the remind message

  POST /api/users

    when the payload is correct

      √ should return a 201 status and the confirmation message

  POST /api/users/:userName/favorites

    √ should add a movie id to user1's favorites 

  Movies endpoint

    GET /api/movies/tmdb/upcoming

      √ should return 20 upcoming movies and a status 200 (100ms)

    GET /api/movies/tmdb/toprated

      √ should return 20 topratedmovies and a status 200 (73ms)

    GET /api/movies/tmdb/nowplaying

      √ should return 20 nowplayingmovies and a status 200 (70ms)

    GET /api/movies/tmdb/popular

      √ should return 20 popularmovies and a status 200 (56ms)

    GET /api/movies/tmdb/movies/:id

      √ should return the detail of one movie (56ms)

      GET /api/movies/tmdb/movies/:id/reviews

        √ should return the reviews of one movie (63ms)

  People endpoint

    GET /api/people/tmdb/popular

      √ should return 20 people who are popular and a status 200 (63ms)

    GET /api/people/tmdb/people/:id

      √ should return the detail of one people (67ms)

    GET /api/people/tmdb/:id/images

      √ should return the people's images (57ms)

  TV endpoint

    GET /api/TV/tmdb/popular

      √ should return 20 TVs and a status 200 (72ms)

    GET /api/TV/tmdb/TV/:id

      √ should return teh details of a TV (126ms)

    GET /api/TV/tmdb/TV/:id

      √ should return the details of a TV (59ms)

    GET /api/TV/tmdb/TV/:id/similar

      √ should return the similar images of a TV (59ms)

    GET /api/TV/tmdb/TV/:id/reviews

      √ should return the reviews of a TV (59ms)

    GET /api/TV/tmdb/TV/:id/genres

      √ should return the genre of a TV (58ms)

  Genres endpoint

    GET /api/genres

      √ should return 4 genres and a status 200

    GET /api/genres/tmdb/movies

      √ should return 4 genres of movies and a status 200

  23 passing (5s)


## Deployments.

https://movies-api-staging-ysr-bd2ee9ec2b7c.herokuapp.com/api/movies/


## Independent Learning (if relevant)
Coveralls web service: https://coveralls.io/gitlab/SIRUI125/moviesapp-ci

I learn the Istanbul to show the code coverage and learn how to publish the reports on the Coveralls web service.

We should use npm install --save-dev coveralls and add - npm run coveralls to our test_api in gitlab-ci.yml.

The website I learn the coverall: https://github.com/jtwebman/coveralls-next

## Other related links

 HEROKU Staging:https://dashboard.heroku.com/apps/movies-api-staging-ysr

 Gitlab:https://gitlab.com/SIRUI125/agile-ca2



