import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";


const expect = chai.expect;
let db;

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); 
  });
  
  
  describe("GET /api/movies/tmdb/upcoming ", () => {
    it("should return 20 upcoming movies and a status 200", () => {
      return request(api)
        .get("/api/movies/tmdb/upcoming")
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
        })
        .catch(err => {
          console.error("Test failed with error: ", err.message);
          throw err; 
        });
    });
  });
  
    describe("GET /api/movies/tmdb/toprated ", () => {
      it("should return 20 topratedmovies and a status 200", () => {
      return request(api)
          .get("/api/movies/tmdb/toprated")
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body.results).to.be.a("array");
            expect(res.body.results.length).to.equal(20);
          
          });
      });
    });
    describe("GET /api/movies/tmdb/nowplaying ", () => {
      it("should return 20 nowplayingmovies and a status 200", () => {
      return request(api)
          .get("/api/movies/tmdb/nowplaying")
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body.results).to.be.a("array");
            expect(res.body.results.length).to.equal(20);
          
          });
      });
    });
    describe("GET /api/movies/tmdb/popular ", () => {
      it("should return 20 popularmovies and a status 200", () => {
        return request(api)
          .get("/api/movies/tmdb/popular")
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body.results).to.be.a("array");
            expect(res.body.results.length).to.equal(20);
          });
      });
    });
    describe("GET /api/movies/tmdb/movies/:id ", () => {
     
      it("should return the detail of one movie", () => {
        return request(api)
          .get(`/api/movies/tmdb/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movies[0].title);
          });
      });
    describe("GET /api/movies/tmdb/movies/:id/reviews ", () => {
      it("should return the reviews of one movie", () => {
        return request(api)
          .get(`/api/movies/tmdb/movies/${movies[0].id}/reviews`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("id", movies[0].id);
            expect(res.body.results).to.be.a("array");
          });
      });
    });
    
  });
});
