import chai from "chai";
import request from "supertest";
import api from "../../../../index";
import TV from "../../../../seedData/TV";
import TVModel from "../../../../api/TV/TVModel";


const expect = chai.expect;
const mongoose = require("mongoose");
let db;

describe("TV endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await TVModel.deleteMany();
      await TVModel.collection.insertMany(TV);
    } catch (err) {
      console.error(`failed to Load TV Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); 
  });

describe("GET /api/TV/tmdb/popular ", () => {
    it("should return 20 TVs and a status 200", () => {
      return request(api)
        .get("/api/TV/tmdb/popular")
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
        });
    });
  });
  describe("GET /api/TV/tmdb/TV/:id ", () => {
    it("should return teh details of a TV", () => {
      return request(api)
        .get(`/api/TV/tmdb/TV/${TV[0].id}`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
            expect(res.body).to.have.property("name", TV[0].name);
        });
    });
  }); 
  describe("GET /api/TV/tmdb/TV/:id ", () => {
    it("should return the details of a TV", () => {
      return request(api)
        .get(`/api/TV/tmdb/TV/${TV[0].id}`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
            expect(res.body).to.have.property("name", TV[0].name);
        });
    });
  }); 
  describe("GET /api/TV/tmdb/TV/:id/similar ", () => {
    it("should return the similar images of a TV", () => {
      return request(api)
        .get(`/api/TV/tmdb/TV/119051/similar`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
        });
    });
  }); 

  describe("GET /api/TV/tmdb/TV/:id/reviews ", () => {
    it("should return the reviews of a TV", () => {
      return request(api)
        .get(`/api/TV/tmdb/TV/119051/reviews`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
            expect(res.body.results[0]).to.have.property("author", "UtileDulci");
        });
    });
  }); 
  describe("GET /api/TV/tmdb/TV/:id/genres ", () => {
    it("should return the genre of a TV", () => {
      return request(api)
        .get(`/api/TV/tmdb/TV/119051/genres`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body.genres[0]).to.have.property("name", "Action & Adventure");  
          
        });
    });
  }); 
});
