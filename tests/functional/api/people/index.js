import chai from "chai";
import request from "supertest";
import api from "../../../../index";
import people from "../../../../seedData/people";
import peopleModel from "../../../../api/people/peopleModel";
const expect = chai.expect;
const mongoose = require("mongoose");
let db;
describe("People endpoint", () => {
    before(() => {
      mongoose.connect(process.env.MONGO_DB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      db = mongoose.connection;
    });
  
    after(async () => {
      try {
        await db.dropDatabase();
      } catch (error) {
        console.log(error);
      }
    });
    beforeEach(async () => {
        try {
          await peopleModel.deleteMany();
          await peopleModel.collection.insertMany(people);
        } catch (err) {
          console.error(`failed to Load user Data: ${err}`);
        }
      });
    afterEach(() => {
      api.close(); // Release PORT 8080
    });

describe("GET /api/people/tmdb/popular ", () => {
    it("should return 20 people who are popular and a status 200", () => {
    return request(api)
        .get("/api/people/tmdb/popular")
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);   
        });
    });
  });
  
  describe("GET /api/people/tmdb/people/:id ", () => {
    it("should return the detail of one people", () => {
      return request(api)
        .get(`/api/people/tmdb/people/2359226`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body).to.have.property("name", "Aya Asahina");
        });
    });
  });
  describe("GET /api/people/tmdb/:id/images", () => {
    it("should return the people's images", () => {
      return request(api)
        .get(`/api/people/tmdb/people/${people[0].id}/images`)
        .set("Accept", "application/json")
        .expect(200)
        .then((res) => {
          expect(res.body).to.have.keys([
            "id",
            "profiles",
          ]);
        });
    });
  });
});
