import chai from "chai";
import request from "supertest";
import api from "../../../../index";
import genres from "../../../../seedData/genres";
import genresModel from "../../../../api/genres/genresModel";
const expect = chai.expect;
const mongoose = require("mongoose");
let db;
describe("Genres endpoint", () => {
    before(() => {
      mongoose.connect(process.env.MONGO_DB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      db = mongoose.connection;
    });
  
    after(async () => {
      try {
        await db.dropDatabase();
      } catch (error) {
        console.log(error);
      }
    });
    beforeEach(async () => {
        try {
          await genresModel.deleteMany();
          await genresModel.collection.insertMany(genres);
        } catch (err) {
          console.error(`failed to Load user Data: ${err}`);
        }
      });
    afterEach(() => {
      api.close(); 
    });
    describe("GET /api/genres ", () => {
        it("should return 4 genres and a status 200", () => {
         return request(api)
            .get("/api/genres")
            .set("Accept", "application/json")
            .expect(200)
            .then((res) => {
              expect(res.body).to.be.a("array");
              expect(res.body.length).to.equal(4);
            });
        });
      });
      describe("GET /api/genres/tmdb/movies ", () => {
        it("should return 4 genres of movies and a status 200", () => {
         return request(api)
            .get("/api/genres")
            .set("Accept", "application/json")
            .expect(200)
            .then((res) => {
              expect(res.body).to.be.a("array");
              expect(res.body.length).to.equal(4);
            
            });
        });
      });  
      
});

